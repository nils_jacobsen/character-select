package com.company;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CharacterTest {

    Ranger testRanger;
    Warrior testWarrior;
    Mage testMage;
    Rogue testRogue;

    @BeforeEach
    void setUp() {
        testRanger = new Ranger("test-ranger");
        testWarrior = new Warrior("test-warrior");
        testMage = new Mage("test-mage");
        testRogue = new Rogue("test-rogue");
    }

    @AfterEach
    void tearDown() {
        testRanger = null;
        testWarrior = null;
        testMage = null;
        testRogue = null;
    }

    @Test
    void isCharacter_startingLevel_one() {
        assertEquals(testRanger.level, 1);
    }

    @Test
    void doesCharacters_LevelUp_oneLevel() {
        testRanger.levelUp();

        assertEquals(testRanger.level, 2);
    }

    @Test
    void isRangers_InitialPrimaryAttributes_correct() {
        assertEquals(testRanger.bpa.dexterity, 7);
        assertEquals(testRanger.bpa.intelligence, 1);
        assertEquals(testRanger.bpa.strength, 1);
    }

    @Test
    void isWarriors_InitialPrimaryAttributes_correct() {
        assertEquals(testWarrior.bpa.dexterity, 2);
        assertEquals(testWarrior.bpa.intelligence, 1);
        assertEquals(testWarrior.bpa.strength, 5);
    }

    @Test
    void isMages_InitialPrimaryAttributes_correct() {
        assertEquals(testMage.bpa.dexterity, 1);
        assertEquals(testMage.bpa.intelligence, 8);
        assertEquals(testMage.bpa.strength, 1);
    }

    @Test
    void isRogue_InitialPrimaryAttributes_correct() {
        assertEquals(testRogue.bpa.dexterity, 6);
        assertEquals(testRogue.bpa.intelligence, 1);
        assertEquals(testRogue.bpa.strength, 2);
    }

    @Test
    void isRangers_PrimaryAttributesLevelUpGain_correct() {
        testRanger.levelUp();
        assertEquals(testRanger.bpa.dexterity, 12);
        assertEquals(testRanger.bpa.intelligence, 2);
        assertEquals(testRanger.bpa.strength, 2);
    }

    @Test
    void isWarriors_PrimaryAttributesLevelUpGain_correct() {
        testWarrior.levelUp();
        assertEquals(testWarrior.bpa.dexterity, 4);
        assertEquals(testWarrior.bpa.intelligence, 2);
        assertEquals(testWarrior.bpa.strength, 8);
    }

    @Test
    void isMages_PrimaryAttributesLevelUpGain_correct() {
        testMage.levelUp();
        assertEquals(testMage.bpa.dexterity, 2);
        assertEquals(testMage.bpa.intelligence, 13);
        assertEquals(testMage.bpa.strength, 2);
    }

    @Test
    void isRogue_PrimaryAttributesLevelUpGain_correct() {
        testRogue.levelUp();
        assertEquals(testRogue.bpa.dexterity, 10);
        assertEquals(testRogue.bpa.intelligence, 2);
        assertEquals(testRogue.bpa.strength, 3);
    }
}