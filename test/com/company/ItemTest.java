package com.company;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ItemTest {

    Ranger testRanger;
    Warrior testWarrior;
    Mage testMage;
    Rogue testRogue;
    Weapon weapon1;
    Weapon weapon2;
    Weapon weapon3;
    Armor armor1;
    Armor armor2;
    Armor armor3;


    @BeforeEach
    void setUp() {
        testRanger = new Ranger("test-ranger");
        testWarrior = new Warrior("test-warrior");
        testMage = new Mage("test-mage");
        testRogue = new Rogue("test-rogue");
        weapon1 = new Weapon(SLOT.WEAPON, "test-weapon",1, WEAPONTYPE.STAFF, 10,2);
        weapon2 = new Weapon(SLOT.WEAPON, "test-weapon",2, WEAPONTYPE.BOW, 100,1);
        weapon3 = new Weapon(SLOT.WEAPON, "test-weapon",1, WEAPONTYPE.BOW, 100,1);
        armor1 = new Armor(SLOT.HEAD, "test-armor", 2, ARMORTYPE.LEATHER, 0,0,0);
        armor2 = new Armor(SLOT.HEAD, "test-armor", 1, ARMORTYPE.PLATE, 0,0,0);
        armor3 = new Armor(SLOT.HEAD, "test-armor", 1, ARMORTYPE.LEATHER, 0,0,10);
    }

    @AfterEach
    void tearDown() {
        testRanger = null;
        testWarrior = null;
        testMage = null;
        testRogue = null;
        weapon1 = null;
        weapon2 = null;
        weapon3 = null;
        armor1 = null;
        armor2 = null;
        armor3 = null;
    }


    @Test
    void isWeapon_notEquipping_ifCharactersLevelTooLow() {

        assertThrows(InvalidWeaponException.class, () -> {
            testRanger.equipWeapon(weapon2);
        });

    }

    @Test
    void isArmor_notEquipping_ifCharactersLevelTooLow() {

        assertThrows(InvalidArmorException.class, () -> {
            testRanger.equipArmor(armor1);
        });

    }


    @Test
    void isWeapon_notEquipping_ifWeaponTypeIsWrong() {

        assertThrows(InvalidWeaponException.class, () -> {
            testRanger.equipWeapon(weapon1);
        });

    }

    @Test
    void isArmor_notEquipping_ifArmorTypeIsWrong() {

        assertThrows(InvalidArmorException.class, () -> {
            testRanger.equipArmor(armor2);
        });

    }

    @Test
    void isWeapon_Equipping_ifValid() throws InvalidWeaponException {

        testRanger.equipWeapon(weapon3);
        boolean isEquipped = true;
        if(testRanger.equipmentSlots.get(SLOT.WEAPON) != null) {
            isEquipped = true;
        }else{
            isEquipped = false;
        }
        assertTrue(isEquipped);
    }

    @Test
    void isArmor_Equipping_ifValid() throws InvalidArmorException {

        testRanger.equipArmor(armor3);
        boolean isEquipped = true;
        if(testRanger.equipmentSlots.get(SLOT.HEAD) != null) {
            isEquipped = true;
        }else{
            isEquipped = false;
        }
        assertTrue(isEquipped);
    }

    @Test
    void isDps_forCharacterWithoutEquippedWeapon_one() {

        double dps = testRanger.calculateDps();

        assertEquals(dps, 1);
    }

    @Test
    void isDps_forCharacterWithEquippedWeapon_CalculatedCorrectly() throws InvalidWeaponException {

        testRanger.equipWeapon(weapon3);
        double dps = testRanger.calculateDps();
        assertEquals(dps, 107);
    }

    @Test
    void isDps_forCharacterWithEquippedWeaponAndArmor_CalculatedCorrectly() throws InvalidWeaponException, InvalidArmorException {

        testRanger.equipWeapon(weapon3);
        testRanger.equipArmor(armor3);
        testRanger.tpa = testRanger.setTPA();
        double dps = testRanger.calculateDps();
        assertEquals(dps, 117);
    }

}