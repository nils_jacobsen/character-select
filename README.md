# character-select

## Description
A java console program. 
Junit5 tests.

## Installation
Clone
Build
Run Main Class

## Usage
You can create a character with the role of Warrior, Mage, Ranger or Rogue and give it a name. Your character will begin at level 1 and will be able equip a piece of armor (with role-specific restrictions), display your stats and level up. When you reach level 5 you can equip a weapon(also restricted to roles). When you level up or equip armor you gain more attributes which will impact your characters damage per second. Some roles use different attributes as a damage modifier (Strength mods the Warrior etc).

## Author
gitlab.com/nils_jacobsen
