package com.company;

import java.util.HashMap;

public class Mage extends Character{

    public Mage(String name) {
        this.name = name;
        this.bpa = new PrimaryAttributes(1,8,1);
        this.equipmentSlots = new HashMap<>();
        this.tpa = setTPA();
    }

    //Can only equip Cloth armor items that has a required level below your characters current level.
    @Override
    public void equipArmor(Armor armor) throws InvalidArmorException{
        if(armor.type == ARMORTYPE.CLOTH){
            if(armor.requiredLevel <= this.level){
                this.equipmentSlots.put(armor.itemSlot, armor);
                this.tpa = setTPA();
            }else{
                throw new InvalidArmorException("You cannot equip " + armor.name + " until you reach level " + armor.requiredLevel);
            }
        }else{
            throw new InvalidArmorException("You cannot equip " + armor.name + ". Wrong armor type: " + armor.type.toString().toLowerCase());
        }
    }

    //Can only equip Wands and Staffs that has a required level below your characters current level.
    @Override
    public void equipWeapon(Weapon weapon) throws InvalidWeaponException{
        if(weapon.type == WEAPONTYPE.STAFF || weapon.type == WEAPONTYPE.WAND){
            if(weapon.requiredLevel <= this.level) {
                this.equipmentSlots.put(SLOT.WEAPON, weapon);
            }else{
                throw new InvalidWeaponException("You cannot equip " + weapon.name + " until you reach level " + weapon.requiredLevel);
            }
        }else{
            throw new InvalidWeaponException("You cannot equip this " + weapon.type.toString().toLowerCase() +  ". Wrong weapon type");
        }
    }

    @Override
    public void displayStats(){
        this.tpa = setTPA();
        System.out.println("The mighty " + this.getClass().getSimpleName() + ", " + this.name +  "\n" +
                "Level: " + this.level + "\n" +
                "Stats: Strength: " + this.tpa.strength + " Intelligence: " + this.tpa.intelligence + " Dexterity: " + this.tpa.dexterity + "\n" +
                "Dps: " + calculateDps());
    }

    @Override
    public double calculateDps() {

        double weaponDamage = 1;
        double attackSpeed = 1;
        double modifier = 1;

        if (this.equipmentSlots.get(SLOT.WEAPON) instanceof Weapon) {
            Weapon weapon = (Weapon) this.equipmentSlots.get(SLOT.WEAPON);
            weaponDamage = weapon.damage;
            attackSpeed = weapon.attacksPerSecond;
            modifier =  tpa.intelligence*0.01 +1;
        }

        double weaponDps = weaponDamage * attackSpeed;
        return weaponDps * modifier;
    }

    @Override
    public void levelUp(){
        this.bpa = new PrimaryAttributes(this.bpa.strength+1, this.bpa.intelligence+5, this.bpa.dexterity+1);
        level++;
        this.tpa = setTPA();
    }

}
