package com.company;

import java.util.HashMap;
import java.util.Objects;

public abstract class Character {
    protected String name;
    protected int level = 1;
    protected PrimaryAttributes bpa;    //Basic primary attributes. Initiates with different values in each subclass constructor.
    protected PrimaryAttributes tpa;    //Total primary attributes. Use setTPA to add stats from equipped armor to the basic primary attributes.
    protected HashMap<SLOT, Item> equipmentSlots; //Has four SLOTS(enum): Weapon, head, body and legs.

    //Abstract methods overloaded in subclasses Warrior, Rogue, Mage and Ranger
    public abstract void displayStats();
    //Each subclass calculates dps with different attribute as modifier. This method runs when you display your stats.
    public abstract double calculateDps();
    //Each subclass levels up with different attribute changes.
    public abstract void levelUp();
    //Overloaded method sets the restrictions for which weapon and armor types the character can equip.
    public abstract void equipWeapon(Weapon weapon) throws InvalidWeaponException;
    public abstract void equipArmor(Armor armor) throws InvalidArmorException;


    //Uses dynamic casting to get the attributes from armor items in equipmentSlots.
    public PrimaryAttributes setTPA() {
        int str = 0;
        int intel = 0;
        int dex = 0;

        if (this.equipmentSlots.get(SLOT.HEAD) instanceof Armor helmet) {
            str += helmet.attributes.strength;
            intel += helmet.attributes.intelligence;
            dex += helmet.attributes.dexterity;
        }
        if (this.equipmentSlots.get(SLOT.BODY) instanceof Armor bodyArmor) {
            str += bodyArmor.attributes.strength;
            intel += bodyArmor.attributes.intelligence;
            dex += bodyArmor.attributes.dexterity;
        }
        if (this.equipmentSlots.get(SLOT.LEGS) instanceof Armor pants) {
            str += pants.attributes.strength;
            intel += pants.attributes.intelligence;
            dex += pants.attributes.dexterity;
        }

        return this.tpa = new PrimaryAttributes(this.bpa.strength+str, this.bpa.intelligence+intel, this.bpa.dexterity+dex);
    }

}
