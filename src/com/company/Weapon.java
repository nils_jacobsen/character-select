package com.company;

public class Weapon extends Item{

    WEAPONTYPE type;
    double damage;
    double attacksPerSecond;

    public Weapon(SLOT weaponSlot, String name, int requiredLevel, WEAPONTYPE type, double damage, double attacksPerSecond) {
        this.name = name;
        this.requiredLevel = requiredLevel;
        this.itemSlot = weaponSlot;
        this.type = type;
        this.damage = damage;
        this.attacksPerSecond = attacksPerSecond;
    }
}
