package com.company;

public class InvalidArmorException extends Exception{

    public InvalidArmorException(String message){
        super(message);
    }

}
