package com.company;

public class PrimaryAttributes {

    protected int strength;
    protected int intelligence;
    protected int dexterity;

    public PrimaryAttributes(int strength, int intelligence, int dexterity) {
        this.strength = strength;
        this.intelligence = intelligence;
        this.dexterity = dexterity;
    }
}
