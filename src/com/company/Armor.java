package com.company;

public class Armor extends Item{

    ARMORTYPE type;
    PrimaryAttributes attributes;

    public Armor(SLOT armorSlot, String name, int requiredLevel, ARMORTYPE type, int strength, int intelligence, int dexterity) {

        this.requiredLevel = requiredLevel;
        this.name = name;
        this.type = type;
        this.itemSlot = armorSlot;
        this.attributes = new PrimaryAttributes(strength, intelligence, dexterity);

    }
}
