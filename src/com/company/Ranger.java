package com.company;

import java.util.HashMap;

public class Ranger extends Character{

    public Ranger(String name) {
        this.name = name;
        this.bpa = new PrimaryAttributes(1,1,7);
        this.equipmentSlots = new HashMap<>();
        this.tpa = setTPA();
    }

    //Can only equip Leather and Mail armor items that has a required level below your characters current level.
    @Override
    public void equipArmor(Armor armor) throws InvalidArmorException{
        if(armor.type == ARMORTYPE.LEATHER || armor.type == ARMORTYPE.MAIL){
            if(armor.requiredLevel <= this.level){
                this.equipmentSlots.put(armor.itemSlot, armor);
                this.tpa = setTPA();
            }else{
                throw new InvalidArmorException("You cannot equip " + armor.name + " until you reach level " + armor.requiredLevel);
            }
        }else{
            throw new InvalidArmorException("You cannot equip " + armor.name + ". Wrong armor type: " + armor.type.toString().toLowerCase());
        }
    }

    //Can only equip Bows that has a required level below your characters current level.
    @Override
    public void equipWeapon(Weapon weapon) throws InvalidWeaponException{
        if(weapon.type == WEAPONTYPE.BOW){
            if(weapon.requiredLevel <= this.level) {
                this.equipmentSlots.put(SLOT.WEAPON, weapon);
            }else{
                throw new InvalidWeaponException("You cannot equip " + weapon.name + " until you reach level " + weapon.requiredLevel);
            }
        }else{
            throw new InvalidWeaponException("You cannot equip this " + weapon.type.toString().toLowerCase() +  ". Wrong weapon type");
        }
    }

    public void displayStats(){
        this.tpa = setTPA();
        System.out.println("The mighty " + this.getClass().getSimpleName() + ", " + this.name +  "\n" +
                "Level: " + this.level + "\n" +
                "Stats: Strength: " + this.tpa.strength + " Intelligence: " + this.tpa.intelligence + " Dexterity: " + this.tpa.dexterity + "\n" +
                "Dps: " + calculateDps());
    }

    @Override
    public double calculateDps() {

        double weaponDamage = 1;
        double attackSpeed = 1;
        double modifier = 1;

        if (this.equipmentSlots.get(SLOT.WEAPON) instanceof Weapon weapon) {
            weaponDamage = weapon.damage;
            attackSpeed = weapon.attacksPerSecond;
            modifier =  tpa.dexterity*0.01 +1;
        }

        double weaponDps = weaponDamage * attackSpeed;
        return weaponDps * modifier;
    }

    @Override
    public void levelUp(){
        this.bpa = new PrimaryAttributes(this.bpa.strength+1, this.bpa.intelligence+1, this.bpa.dexterity+5);
        level++;
        this.tpa = setTPA();
    }


}
