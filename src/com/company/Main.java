package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args){

        //Run main to play!
        playGame(selectCharacter());

    }

    //Hint: The mage is OP.
    public static Character selectCharacter(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter your characters name");
        String name = scanner.next();
        System.out.println("Enter a number to select your characters role \n1:Warrior\n2:Mage\n3:Ranger\n4:Rogue");
        int characterSelection = scanner.nextInt();
        switch (characterSelection){
            case 1:
                return new Warrior(name);
            case 2:
                return new Mage(name);
            case 3:
                return new Ranger(name);
            case 4:
                return new Rogue(name);
        }
        return null;
    }

    //You get error messages if you try to equip the wrong type of armor or weapon.
    public static void playGame(Character character) {
        System.out.println(character.name);
        System.out.println(character.getClass().getSimpleName());

        Weapon weapon1 = new Weapon(SLOT.WEAPON, "Mighty Bow", 5, WEAPONTYPE.BOW, 100, 5);
        Weapon weapon2 = new Weapon(SLOT.WEAPON, "Sturdy Hammer", 5, WEAPONTYPE.HAMMER, 500, 2);
        Weapon weapon3 = new Weapon(SLOT.WEAPON, "Pointy Wand", 5, WEAPONTYPE.WAND, 300, 10);
        Weapon weapon4 = new Weapon(SLOT.WEAPON, "Sneaky Dagger", 5, WEAPONTYPE.DAGGER, 200, 20);
        Armor armor1 = new Armor(SLOT.HEAD, "Bandana", 1, ARMORTYPE.CLOTH, 2, 100, 5);
        Armor armor2 = new Armor(SLOT.HEAD, "Coarse Cap", 1, ARMORTYPE.LEATHER, 5, 0, 40);
        Armor armor3 = new Armor(SLOT.HEAD, "Flimsy Chain Hood", 1, ARMORTYPE.MAIL, 5, 5, 30);
        Armor armor4 = new Armor(SLOT.HEAD, "Sturdy Helmet", 1, ARMORTYPE.PLATE, 50, 5, 10);

        boolean play = true;
        while (play) {
            Scanner scanner = new Scanner(System.in);
            System.out.println("Lets play\nWhat do you want to do?\n1:Level up\n2:Display stats\n3:Equip armor\n4:Equip weapon\n9:Quit");
            int playAction = scanner.nextInt();
            switch (playAction) {
                case 1:
                    character.levelUp();
                    break;
                case 2:
                    character.displayStats();
                    break;
                case 3:
                    System.out.println("1:Cloth 2:Leather 3:Mail 4:Plate");
                    int arm = scanner.nextInt();
                    try {
                        switch (arm) {
                            case 1:
                            character.equipArmor(armor1);
                            break;
                            case 2:
                            character.equipArmor(armor2);
                            break;
                            case 3:
                            character.equipArmor(armor3);
                            break;
                            case 4:
                            character.equipArmor(armor4);
                            break;
                        }
                    } catch (InvalidArmorException invalidArmorException) {
                        System.out.println(invalidArmorException.getMessage());
                    }
                    break;
                case 4:
                    System.out.println("1:Bow 2:Hammer 3:Wand 4:Dagger");
                    int wep = scanner.nextInt();
                    try {
                        switch (wep) {
                            case 1:
                                character.equipWeapon(weapon1);
                                break;
                            case 2:
                                character.equipWeapon(weapon2);
                                break;
                            case 3:
                                character.equipWeapon(weapon3);
                                break;
                            case 4:
                                character.equipWeapon(weapon4);
                                break;
                        }
                    } catch (InvalidWeaponException invalidWeaponException) {
                        System.out.println(invalidWeaponException.getMessage());
                    }
                    break;
                case 9:
                    play = false;
            }
        }
    }
}
