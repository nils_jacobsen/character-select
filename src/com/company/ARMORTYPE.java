package com.company;

public enum ARMORTYPE {
    CLOTH,
    LEATHER,
    MAIL,
    PLATE
}
